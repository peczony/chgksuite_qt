#!/usr/bin/env python
# -*- coding: utf-8 -*-
from chgksuite_qt.gui import app


def main():
    app()


if __name__ == "__main__":
    main()
